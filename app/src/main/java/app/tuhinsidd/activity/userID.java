package app.tuhinsidd.activity;

/**
 * Created by MOTIBA on 17/04/16.
 */
public class userID {
    private static userID instance;

    // Global variable
    private int data;
    private String UserHostel="";
    private String first_name="";
    private String last_name="";
    private String contact_no="";
    private String entryNumber="";
    private String department="";
    private String room_num="";
    private String user_type="";

    // Restrict the constructor from being instantiated
    private userID(){}

    public void setData(int d,String hostel,String first_name,String last_name,String contact_no,String OfficeNumber,String department
    ,String room_num,String user_type){
        this.data=d;
        this.UserHostel=hostel;
        this.first_name=first_name;
        this.last_name=last_name;
        this.contact_no=contact_no;
        this.entryNumber=OfficeNumber;
        this.department=department;
        this.room_num=room_num;
        this.user_type=user_type;

    }
    public String getfirstname(){
        return this.first_name;
    }

    public String getlastname(){
        return this.last_name;
    }

    public String getContactNumber(){
        return this.contact_no;
    }

    public String getEntryNumber(){
        return this.entryNumber;

    }
    public int getData(){
        return this.data;

    }

    public String getDepartment(){
        return this.department;
    }

    public String getroom_num(){
        return this.room_num;
    }
    public String getUser_type(){
        return this.user_type;
    }

    public String getHostel(){
        return this.UserHostel;
    }

    public static synchronized userID getInstance(){
        if(instance==null){
            instance=new userID();
        }
        return instance;
    }
}
