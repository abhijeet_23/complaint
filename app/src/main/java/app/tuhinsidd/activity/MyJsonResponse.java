package app.tuhinsidd.activity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by MOTIBA on 23/02/16.
 */
public class MyJsonResponse {
    @SerializedName("user")
    userDetails user;
    @SerializedName("success")
    boolean success;
    @SerializedName("resolved")
    List<complaints> resolvedComplaints;
    @SerializedName("active")
    List<complaints> activeComplaints;

    public class userDetails{
        @SerializedName("last_name")
        String last_name;
        @SerializedName("reset_password_key")
        String reset_password_key;
        @SerializedName("registration key")
        String registration_key;
        @SerializedName("id")
        int id;
        @SerializedName("hostel")
        String hostel;
        @SerializedName("first_name")
        String first_name;
        @SerializedName("office_address")
        String office_address;
        @SerializedName("entry_no")
        String entry_no;
        @SerializedName("office_number")
        String office_number;
        @SerializedName("email")
        String email;
        @SerializedName("usertype")
        String usertype;
        @SerializedName("registration_id")
        String registration_id;
        @SerializedName("contact_no")
        String contact_no;
        @SerializedName("department")
        String department;
        @SerializedName("room_no")
        String room_no;
        @SerializedName("password")
        String password;
    }

    public class complaints{
        @SerializedName("complaint_type")
        String complaint_type;
        @SerializedName("category")
        String category;
        @SerializedName("complaint_location")
        String complaint_location;
        @SerializedName("modified_by")
        String modified_by;
        @SerializedName("sub_category")
        String sub_category;
        @SerializedName("picture")
        String picture;
        @SerializedName("is_active")
        String is_active;
        @SerializedName("upvote_ids")
        String upvote_ids;
        @SerializedName("created_by")
        String created_by;
        //        @SerializedName("created_on")
//        Date created_on;
//        @SerializedName("modified_on")
//        Date modified_on;
        @SerializedName("status")
        String status;
        @SerializedName("upvotes")
        String upvotes;
        @SerializedName("hostel")
        String hostel;
        @SerializedName("id")
        String id;
        @SerializedName("description")
        String description;
    }
}