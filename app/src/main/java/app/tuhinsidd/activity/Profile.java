package app.tuhinsidd.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.yalantis.guillotine.animation.GuillotineAnimation;


import app.tuhinsidd.R;
import app.tuhinsidd.image_utils.GOTOConstants;
import app.tuhinsidd.image_utils.ImageCropActivity;
import app.tuhinsidd.image_utils.PicModeSelectDialogFragment;
import butterknife.Bind;
import butterknife.ButterKnife;
//import butterknife.InjectView;

public class Profile extends AppCompatActivity implements PicModeSelectDialogFragment.IPicModeSelectListener{
    private static final long RIPPLE_DURATION = 250;
    CircularImageView propic;

    public static final String TAG = "ImageViewActivity";
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_UPDATE_PIC = 0x1;
    private String imgUri;

    String admin_cat;

    private Button button;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.root)
    FrameLayout root;
    @Bind(R.id.content_hamburger)
    View contentHamburger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int x=0;
        super.onCreate(savedInstanceState);
        switch(x)
        {
            case 0:
                setContentView(R.layout.activity_profile);
                break;
            case 1:
                setContentView(R.layout.activity_profile_faculty);
        }
        //setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }

        propic=(CircularImageView)findViewById(R.id.propic);
        propic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddProfilePicDialog();
            }
        });


        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
        LinearLayout lt=(LinearLayout)findViewById(R.id.profile_group);
        final ImageView ii=(ImageView)findViewById(R.id.pro);
        final ImageView ii1=(ImageView)findViewById(R.id.make);
        final ImageView ii2=(ImageView)findViewById(R.id.my);
        final ImageView ii3=(ImageView)findViewById(R.id.actt);
        final ImageView ii4=(ImageView)findViewById(R.id.log);
        final ImageView ii5=(ImageView)findViewById(R.id.laf);
        ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_selected));


        final TextView tv=(TextView)findViewById(R.id.profile);
        final TextView tv4=(TextView)findViewById(R.id.make1);
        final TextView tv2=(TextView)findViewById(R.id.feed);
        final TextView tv1=(TextView)findViewById(R.id.act);
        final TextView tv3=(TextView)findViewById(R.id.setting);
        final TextView tv5=(TextView)findViewById(R.id.laff);
        tv.setTextColor(getResources().getColor(R.color.selected_item_color));

        lt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.guillo), "You are already here !",
                        Snackbar.LENGTH_LONG);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.gu));
                snackbar.show();
                Log.d("Profile", "profile");

                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_selected));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

            }
        });
        LinearLayout lt1=(LinearLayout)findViewById(R.id.make_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Snackbar.make(findViewById(R.id.guillo), "Make a complaint !", Snackbar.LENGTH_LONG)
              //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp_selected));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i=new Intent(getBaseContext(),MakeComplaint.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt4=(LinearLayout)findViewById(R.id.feed_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Snackbar.make(findViewById(R.id.guillo), "My Complaints !", Snackbar.LENGTH_LONG)
             //           .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed_selected));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i=new Intent(getBaseContext(),MyComplaints.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt2=(LinearLayout)findViewById(R.id.settings_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getBaseContext(),LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();

            }
        });
        LinearLayout lt3=(LinearLayout)findViewById(R.id.activity_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Snackbar.make(findViewById(R.id.guillo), "Home !", Snackbar.LENGTH_LONG)
              //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity_active));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i=new Intent(getBaseContext(),Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt5=(LinearLayout)findViewById(R.id.laf_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Snackbar.make(findViewById(R.id.guillo), "Home !", Snackbar.LENGTH_LONG)
                //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp_selected));

                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.selected_item_color));

                Intent i=new Intent(getBaseContext(),Lost_and_found.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });

        button = (Button) findViewById(R.id.admin);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showInputDialog();
            }
        });

        TextView t=(TextView)findViewById(R.id.name);
        t.setText(userID.getInstance().getfirstname()+" "+userID.getInstance().getlastname());

        TextView t1=(TextView)findViewById(R.id.hostel);
        t1.setText(userID.getInstance().getroom_num()+", "+userID.getInstance().getHostel());

        TextView t2=(TextView)findViewById(R.id.dept);
        t2.setText(userID.getInstance().getDepartment());

        TextView t3=(TextView)findViewById(R.id.phn);
        t3.setText(userID.getInstance().getContactNumber());

        TextView t4=(TextView)findViewById(R.id.entry);
        t4.setText(userID.getInstance().getEntryNumber());

    }
    PopupWindow popUp;

    protected void showInputDialog() {

        // get prompts.xml view

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View promptView = layoutInflater.inflate(R.layout.dialoog_admin, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        MaterialSpinner spinner = (MaterialSpinner) promptView.findViewById(R.id.spinner_cat);
        spinner.setItems("Hostel", "Institute");
        setsubcategory("Hostel",promptView);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                admin_cat = item;
               // Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                setsubcategory(item,promptView);
            }
        });

        // setup a dialog window

        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showInputDialog1();
                        dialog.cancel();
//
                }}).setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog


        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
                }
    protected void showInputDialog1() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View promptView = layoutInflater.inflate(R.layout.dialog1, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        final AlertDialog alert = alertDialogBuilder.create();
        alert.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.cancel();
            }
        }, 2 * 1000);

    }
    public void setsubcategory(String s,View v) {
        if (s.equalsIgnoreCase("Hostel")) {
            MaterialSpinner spinner1 = (MaterialSpinner) v.findViewById(R.id.spinner_hostel);
            spinner1.setItems("Kumaon", "Jwalamukhi", "Aravali", "Karakoram", "Nilgiri", "Zanskar", "Shivalik", "VIndhyachal", "Satpura", "Udaigiri","Girnar", "Himadri", "Kailash");
            spinner1.setSelectedIndex(0);
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                   // Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                }
            });
        }

        else if (s.equalsIgnoreCase("Institute")) {
            MaterialSpinner spinner1 = (MaterialSpinner) v.findViewById(R.id.spinner_hostel);
            spinner1.setItems("");
            spinner1.setSelectedIndex(0);
        }
    }

    @SuppressLint("InlinedApi")
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1234);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == REQUEST_CODE_UPDATE_PIC) {
            if (resultCode == RESULT_OK) {
                String imagePath = result.getStringExtra(GOTOConstants.IntentExtras.IMAGE_PATH);
                showCroppedImage(imagePath);
            } else if (resultCode == RESULT_CANCELED) {
                //TODO : Handle case
            } else {
                String errorMsg = result.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showCroppedImage(String mImagePath) {
        if (mImagePath != null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(mImagePath);
            propic.setImageBitmap(myBitmap);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showAddProfilePicDialog() {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(this);
        dialogFragment.show(getFragmentManager(), "picModeSelector");
    }


    private void actionProfilePic(String action) {
        Intent intent = new Intent(this, ImageCropActivity.class);
        intent.putExtra("ACTION", action);
        startActivityForResult(intent, REQUEST_CODE_UPDATE_PIC);
    }


    @Override
    public void onPicModeSelected(String mode) {
        String action = mode.equalsIgnoreCase(GOTOConstants.PicModes.CAMERA) ? GOTOConstants.IntentExtras.ACTION_CAMERA : GOTOConstants.IntentExtras.ACTION_GALLERY;
        actionProfilePic(action);
    }
    @Override
    public void onBackPressed()
    {
        Intent i=new Intent(this,Home.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        finish();
    }
}
