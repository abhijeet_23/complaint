package app.tuhinsidd.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.yalantis.guillotine.animation.GuillotineAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import app.tuhinsidd.R;
import app.tuhinsidd.adapter.Comment_Adapter;
import app.tuhinsidd.adapter.Comment_Item;
import app.tuhinsidd.widget.CanaroTextView;
import butterknife.Bind;
import butterknife.ButterKnife;
//import butterknife.InjectView;

public class MyComplaints extends AppCompatActivity {
    private static final long RIPPLE_DURATION = 250;



    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.root)
    FrameLayout root;
    @Bind(R.id.content_hamburger)
    View contentHamburger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycomplaints);
        ButterKnife.bind(this);



        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
        LinearLayout lt=(LinearLayout)findViewById(R.id.profile_group);
        final ImageView ii=(ImageView)findViewById(R.id.pro);
        final ImageView ii1=(ImageView)findViewById(R.id.make);
        final ImageView ii2=(ImageView)findViewById(R.id.my);
        final ImageView ii3=(ImageView)findViewById(R.id.actt);
        final ImageView ii4=(ImageView)findViewById(R.id.log);
        final ImageView ii5=(ImageView)findViewById(R.id.laf);
        ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed_selected));


        final TextView tv=(TextView)findViewById(R.id.profile);
        final TextView tv4=(TextView)findViewById(R.id.make1);
        final TextView tv2=(TextView)findViewById(R.id.feed);
        final TextView tv1=(TextView)findViewById(R.id.act);
        final TextView tv3=(TextView)findViewById(R.id.setting);
        final TextView tv5=(TextView)findViewById(R.id.laff);
        tv2.setTextColor(getResources().getColor(R.color.selected_item_color));

        lt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Snackbar snackbar = Snackbar.make(findViewById(R.id.guillo), "You are already here !",
                //        Snackbar.LENGTH_LONG);
                //View snackBarView = snackbar.getView();
                //snackBarView.setBackgroundColor(getResources().getColor(R.color.gu));
                //snackbar.show();
                Log.d("Profile", "profile");

                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_selected));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));


                Intent i=new Intent(getBaseContext(),Profile.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();


            }
        });
        LinearLayout lt1=(LinearLayout)findViewById(R.id.make_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Snackbar.make(findViewById(R.id.guillo), "Make a complaint !", Snackbar.LENGTH_LONG)
               //         .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp_selected));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));


                Intent i=new Intent(getBaseContext(),MakeComplaint.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt4=(LinearLayout)findViewById(R.id.feed_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.guillo), "You are already here !",
                        Snackbar.LENGTH_LONG);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.gu));
                snackbar.show();

                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed_selected));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

            }
        });
        LinearLayout lt2 = (LinearLayout) findViewById(R.id.settings_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getBaseContext(),LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();

            }
        });
        LinearLayout lt3 = (LinearLayout) findViewById(R.id.activity_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Snackbar.make(findViewById(R.id.guillo), "Home !", Snackbar.LENGTH_LONG)
                //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity_active));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));


                Intent i=new Intent(getBaseContext(),Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });

        LinearLayout lt5=(LinearLayout)findViewById(R.id.laf_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Snackbar.make(findViewById(R.id.guillo), "Home !", Snackbar.LENGTH_LONG)
                //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp_selected));

                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.selected_item_color));

                Intent i=new Intent(getBaseContext(),Lost_and_found.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Active", Active.class)
                .add("Resolved", Resolved.class)
                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = getString(R.string.ip)+"default/myComplaints.json/"+userID.getInstance().getData();
        final ArrayList<JSONObject> resolved_complaint_list = new ArrayList<>();
        final ArrayList<JSONObject> active_complaint_list = new ArrayList<>();


        StringRequest myReq = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //parsing the json response from the request
                            JSONObject response1 = new JSONObject(response);

                            JSONArray resolved_complaint = response1.getJSONArray("resolved");
                            JSONArray active_complaints = response1.getJSONArray("active");






                            for (int i=0;i<resolved_complaint.length();i++){
                                JSONObject comp=resolved_complaint.getJSONObject(i);
                                JSONObject temp = new JSONObject();
                                //extract info from the jsonobject in this way --abhi
                                //boolean resolve_status = comp.getBoolean("resolve_status");
                                //String posted_on = comp.getString("created_on");

                            }

                            //exception handling
                        }

                         catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                // hide the progress dialog
                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT);
            }
        });
        queue.add(myReq);










    }
    @Override
    public void onBackPressed()
    {
        Intent i=new Intent(this,Home.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        finish();
    }
    public static class Active extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_active, container, false);
            LinearLayout ll=(LinearLayout)rootView.findViewById(R.id.container1);











            for(int i=0;i<3;i++) {
                LinearLayout aula = (LinearLayout) inflater.inflate(R.layout.feed_item, container, false);
                ll.addView(aula);
                final CanaroTextView like=(CanaroTextView)aula.findViewById(R.id.upvote);
                final TextView likenum=(TextView)aula.findViewById(R.id.votenum);
                like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        likenum.setText(String.valueOf(Integer.parseInt(likenum.getText().toString()) + 1));
                        like.setTextColor(getResources().getColor(R.color.blue));
                        like.setEnabled(false);
                    }
                });
                CanaroTextView comment=(CanaroTextView)aula.findViewById(R.id.comment);
                comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showInputDialog();
                    }
                });
                final CanaroTextView resolve=(CanaroTextView)aula.findViewById(R.id.resolve);
                resolve.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resolve.setTextColor(getResources().getColor(R.color.blue));
                        Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.guillo), "Resolve.",
                                Snackbar.LENGTH_LONG);


                            String url = getString(R.string.ip)+"default/appLogin.json?"+"complaint_id="+""+"&user_id="+userID.getInstance().getData();
                            RequestQueue queue = Volley.newRequestQueue(getContext());
                            StringRequest StRequest = new StringRequest(Request.Method.GET,url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // display response
                                            Log.d("Response", response);

                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.d("Error.Response", error.toString());

                                        }
                                    }
                            );
                            queue.add(StRequest);



                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getResources().getColor(R.color.gu));
                        snackbar.show();

                    }
                });
            }
            return rootView;
        }


        private Comment_Adapter Adapter;


        protected void showInputDialog() {
            final List<Comment_Item> stockList = new ArrayList<Comment_Item>();
            stockList.add(new Comment_Item("Tuhinanksu Das", "bjabvsdjabsdbbdhjVDMcbs,jdbcghavs,jchbvaj", "21:30"));
            stockList.add(new Comment_Item("Nirmal Shaju", "a sdbaknsdjasdnmaskdbjhzbdbhmzvmbhdvnshdbvjfb ","22:30"));
            stockList.add(new Comment_Item("Yogesh S", "asjdbzmbv,SDV,nbfshmdb,cnbsdhbsjdbck,sjbdkjvhsdghv","22:35"));
            stockList.add( new Comment_Item("Vivek ", "a,dhbMDSbvhvsmdncb,sjhjgsdjbcskd,sjdbhfsd", "22:45"));
            stockList.add(new Comment_Item("Sid Jain", "sdfbsjdbnhjsbvd,jvb,jsbhdvhjbs,dhv", "22:30"));
            stockList.add(new Comment_Item("Esvar", "sd,jvhb,sjbdvmhbs,djbvs,hbdvkhjbsd,jhbv", "22:30"));

            Comment_Item[] stockArr = new Comment_Item[stockList.size()];
            stockArr = stockList.toArray(stockArr);

            // get prompts.xml view
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            final View promptView = layoutInflater.inflate(R.layout.dialog_comment, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setView(promptView);
            Adapter = new Comment_Adapter(getActivity(), Arrays.asList(stockArr));

            final ListView listView = (ListView) promptView.findViewById(R.id.list);
            listView.setAdapter(Adapter);

            final EditText et=(EditText)promptView.findViewById(R.id.autocomplete_search);

            Button fab = (Button) promptView.findViewById(R.id.button);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String s = et.getText().toString().trim();
                    if (s.equalsIgnoreCase("")) {
                        Snackbar.make(promptView.findViewById(R.id.aa1), "Please enter comment !", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        return;
                    }
                    et.setText("");

                    //System.arraycopy(comments, 0, comments1, 0, comments.length);
                    Calendar c = Calendar.getInstance();
                    //SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                    //String formattedDate = df.format(c.getTime());
                    stockList.add(new Comment_Item("Tuhinanksu Das",s,c.getTime().toString()));
                    //comments1[comments1.length-1]=new Comment_Item("Tuhinanksu Das",s,formattedDate,c.getTime().toString());
                    //Comment_Item[] comments1=new Comment_Item[comments.length+1];
                    Comment_Item[] stockArr1 = new Comment_Item[stockList.size()];
                    stockArr1 = stockList.toArray(stockArr1);
                    Adapter = new Comment_Adapter(getActivity(), Arrays.asList(stockArr1));
                    listView.setAdapter(Adapter);

                }
            });

            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }




}




    public static class Resolved extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_resolved, container, false);
            return rootView;
        }
    }


}
