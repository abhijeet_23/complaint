package app.tuhinsidd.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.yalantis.guillotine.animation.GuillotineAnimation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import app.tuhinsidd.R;
import app.tuhinsidd.image_utils.GOTOConstants;
import app.tuhinsidd.image_utils.ImageCropActivity;
import app.tuhinsidd.image_utils.PicModeSelectDialogFragment;
import butterknife.Bind;
import butterknife.ButterKnife;
//import butterknife.InjectView;

public class MakeComplaint extends AppCompatActivity implements PicModeSelectDialogFragment.IPicModeSelectListener {
    private static final long RIPPLE_DURATION = 250;
    String type = "";
    ImageView imgcomp;

    public static final String TAG = "ImageViewActivity";
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_UPDATE_PIC = 0x1;
    private String imgUri;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.root)
    FrameLayout root;
    @Bind(R.id.content_hamburger)
    View contentHamburger;

    String Complaint_type = "";
    String Category = "";
    String subCategory = "";
    String location = "";
    String description = "";
    String imageUrl = "";
    FloatingActionButton submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makecomplaint);
        ButterKnife.bind(this);

        submit = (FloatingActionButton) findViewById(R.id.fab);

        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                submitComplaint();
            }
        });

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
        LinearLayout lt = (LinearLayout) findViewById(R.id.profile_group);
        final ImageView ii = (ImageView) findViewById(R.id.pro);
        final ImageView ii1 = (ImageView) findViewById(R.id.make);
        final ImageView ii2 = (ImageView) findViewById(R.id.my);
        final ImageView ii3 = (ImageView) findViewById(R.id.actt);
        final ImageView ii4 = (ImageView) findViewById(R.id.log);
        final ImageView ii5 = (ImageView) findViewById(R.id.laf);
        ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp_selected));


        final TextView tv = (TextView) findViewById(R.id.profile);
        final TextView tv4 = (TextView) findViewById(R.id.make1);
        final TextView tv2 = (TextView) findViewById(R.id.feed);
        final TextView tv1 = (TextView) findViewById(R.id.act);
        final TextView tv3 = (TextView) findViewById(R.id.setting);
        final TextView tv5 = (TextView) findViewById(R.id.laff);
        tv4.setTextColor(getResources().getColor(R.color.selected_item_color));

        lt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Profile", "profile");

                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_selected));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i = new Intent(getBaseContext(), Profile.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();


            }
        });
        LinearLayout lt1 = (LinearLayout) findViewById(R.id.make_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.guillo), "You are already here !",
                        Snackbar.LENGTH_LONG);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.gu));
                snackbar.show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp_selected));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));
            }
        });
        LinearLayout lt4 = (LinearLayout) findViewById(R.id.feed_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Snackbar.make(findViewById(R.id.guillo), "My Complaints !", Snackbar.LENGTH_LONG)
                //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed_selected));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i = new Intent(getBaseContext(), MyComplaints.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt2 = (LinearLayout) findViewById(R.id.settings_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();

            }
        });
        LinearLayout lt3 = (LinearLayout) findViewById(R.id.activity_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //    Snackbar.make(findViewById(R.id.guillo), "Home !", Snackbar.LENGTH_LONG)
                //            .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity_active));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));


                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i = new Intent(getBaseContext(), Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt5 = (LinearLayout) findViewById(R.id.laf_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Snackbar.make(findViewById(R.id.guillo), "Home !", Snackbar.LENGTH_LONG)
                //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp_selected));

                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.selected_item_color));

                Intent i = new Intent(getBaseContext(), Lost_and_found.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });

        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.spinner_type);
        spinner.setItems("Individual", "Hostel", "Institute");
        setcategory("Individual");
        Complaint_type = "Individual";
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                type = item;

                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                setcategory(item);
            }
        });
        imgcomp = (ImageView) findViewById(R.id.image1);
        ImageButton ib = (ImageButton) findViewById(R.id.addimage);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddProfilePicDialog();
            }
        });


        EditText spinner11 = (EditText) findViewById(R.id.spinner_location);

        location = spinner11.getText().toString();


        EditText spinner12 = (EditText) findViewById(R.id.spinner_description);

        description = spinner12.getText().toString();


    }
    protected void showInputDialog1() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View promptView = layoutInflater.inflate(R.layout.dialog2, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);
        final AlertDialog alert = alertDialogBuilder.create();
        alert.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.cancel();
            }
        }, 2 * 1000);
    }

    public void setcategory(String s) {
        Complaint_type = s;
        if (s.equalsIgnoreCase("Individual")) {
            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_category);
            setsubcategory("Room Related");
            Category = "Room_Related";
            spinner1.setItems("Room Related", "Ragging", "Other");
            spinner1.setSelectedIndex(0);
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    setsubcategory(item);
                }
            });
        } else if (s.equalsIgnoreCase("Hostel")) {

            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_category);
            spinner1.setItems("Maintenance", "Mess", "Other");
            setsubcategory("Maintenance");
            Category = "Maintenance";
            spinner1.setSelectedIndex(0);
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    setsubcategory(item);
                }
            });
        } else if (s.equalsIgnoreCase("Institute")) {
            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_category);
            spinner1.setItems("CSC/Library", "Eating Outlets", "Sports", "Hospital", "Institute Maintenance", "Institute Electrical", "Academics", "Student Affairs", "Others");
            spinner1.setSelectedIndex(0);
            setsubcategory("Others");
            Category = "Others";
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    setsubcategory(item);
                }
            });
        } else {
            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_category);
            spinner1.setItems();
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                }
            });
        }
    }

    public void setsubcategory(String s) {

        Category = s.replaceAll("\\s+", "");
        if (s.equalsIgnoreCase("Room Related")) {
            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_subcat);
            spinner1.setItems("Electrical", "Civil", "Lan");
            spinner1.setSelectedIndex(0);
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    subCategory = item.replaceAll("\\s+", "");
                }
            });
        } else if (s.equalsIgnoreCase("Maintenance")) {
            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_subcat);
            spinner1.setItems("Corridor", "Washroom", "Common Space");
            spinner1.setSelectedIndex(0);
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    subCategory = item.replaceAll("\\s+", "");
                }
            });
        } else if (s.equalsIgnoreCase("Mess")) {
            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_subcat);
            spinner1.setItems("Menu Variation", "Food Quality", "Hygiene");
            spinner1.setSelectedIndex(0);
            spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                    Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                    subCategory = item.replaceAll("\\s+", "");
                }
            });
        } else if (s.equalsIgnoreCase("Other") || s.equalsIgnoreCase("Ragging") || s.equalsIgnoreCase("Eating Outlets") || s.equalsIgnoreCase("Sports") || s.equalsIgnoreCase("Hospital") || s.equalsIgnoreCase("CSC/Library") || s.equalsIgnoreCase("Institute Maintenance") || s.equalsIgnoreCase("Institute Electrical") || s.equalsIgnoreCase("Academics") || s.equalsIgnoreCase("Student Affairs") || s.equalsIgnoreCase("Others")) {
            MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner_subcat);
            spinner1.setItems("");
            spinner1.setSelectedIndex(0);

        }
    }

    private void showAddProfilePicDialog() {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(this);
        dialogFragment.show(getFragmentManager(), "picModeSelector");
    }


    private void actionProfilePic(String action) {
        Intent intent = new Intent(this, ImageCropActivity.class);
        intent.putExtra("ACTION", action);
        startActivityForResult(intent, REQUEST_CODE_UPDATE_PIC);
    }


    @Override
    public void onPicModeSelected(String mode) {
        String action = mode.equalsIgnoreCase(GOTOConstants.PicModes.CAMERA) ? GOTOConstants.IntentExtras.ACTION_CAMERA : GOTOConstants.IntentExtras.ACTION_GALLERY;
        actionProfilePic(action);
    }

    @SuppressLint("InlinedApi")
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1234);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == REQUEST_CODE_UPDATE_PIC) {
            if (resultCode == RESULT_OK) {
                String imagePath = result.getStringExtra(GOTOConstants.IntentExtras.IMAGE_PATH);
                showCroppedImage(imagePath);
            } else if (resultCode == RESULT_CANCELED) {
                //TODO : Handle case
            } else {
                String errorMsg = result.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showCroppedImage(String mImagePath) {
        if (mImagePath != null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(mImagePath);
            imgcomp.setImageBitmap(myBitmap);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, Home.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        finish();
    }

    public void submitComplaint() {
        //RequestQueue queue = Volley.newRequestQueue(this);
        String url1 =
                getString(R.string.ip) + "default/insertComplaint.json?complaint_type=" + Complaint_type + "&category=" +
                        Category + "&sub_category=" + subCategory + "&hostel=" + userID.getInstance().getHostel() +
                        "&complaint_location=" + location + "&description=" + description + "&picture" + "+imageUrl+&user_id=" + userID.getInstance().getData();

       /* StringRequest myReq = new StringRequest(Request.Method.GET,
                url1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //parsing the json response from the request
                            JSONObject response1 = new JSONObject(response);
                            boolean Success = response1.getBoolean("success");
                            if (Success){
                                Toast.makeText(getApplicationContext(),"Complaint Sumbitted", Toast.LENGTH_SHORT);
                            }
                            else{
                                //alertdialog if the credentials are invalid
                                AlertDialog.Builder alert = new AlertDialog.Builder(MakeComplaint.this);
                                alert.setTitle("Error !! Complaint not Submitted");

                                final String MessageToSubmit="Please enter the details again...";
                                alert.setMessage(MessageToSubmit);

                                alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.cancel();
                                    }
                                });
                                alert.show();
                            }
                            //exception handling
                        } catch (JSONException ex) {
                            Log.e(TAG, "Cannot parse response!!");
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog


            }
        });
        queue.add(myReq);
    }*/


        //String url = getString(R.string.ip)+"default/appLogin.json?email=" + email + "&password=" + password;
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest StRequest = new StringRequest(Request.Method.GET, url1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // display response
                        try {
                            JSONObject n=new JSONObject(response);
                            boolean Success = n.getBoolean("success");
                            if(Success)
                            {
                                showInputDialog1();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());

                    }
                }
        );
        queue.add(StRequest);


    }
}

