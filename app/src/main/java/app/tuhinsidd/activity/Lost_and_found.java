package app.tuhinsidd.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.yalantis.guillotine.animation.GuillotineAnimation;

import java.util.List;

import app.tuhinsidd.R;
import app.tuhinsidd.image_utils.GOTOConstants;
import app.tuhinsidd.image_utils.ImageCropActivity;
import app.tuhinsidd.image_utils.PicModeSelectDialogFragment;
import butterknife.Bind;
import butterknife.ButterKnife;
//import butterknife.InjectView;

public class Lost_and_found extends AppCompatActivity {
    private static final long RIPPLE_DURATION = 250;
    String type="";
    ImageView imgcomp;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.root)
    FrameLayout root;
    @Bind(R.id.content_hamburger)
    View contentHamburger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lost_and_found);
        ButterKnife.bind(this);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
        LinearLayout lt=(LinearLayout)findViewById(R.id.profile_group);
        final ImageView ii=(ImageView)findViewById(R.id.pro);
        final ImageView ii1=(ImageView)findViewById(R.id.make);
        final ImageView ii2=(ImageView)findViewById(R.id.my);
        final ImageView ii3=(ImageView)findViewById(R.id.actt);
        final ImageView ii4=(ImageView)findViewById(R.id.log);
        final ImageView ii5=(ImageView)findViewById(R.id.laf);
        ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp_selected));


        final TextView tv=(TextView)findViewById(R.id.profile);
        final TextView tv4=(TextView)findViewById(R.id.make1);
        final TextView tv2=(TextView)findViewById(R.id.feed);
        final TextView tv1=(TextView)findViewById(R.id.act);
        final TextView tv3=(TextView)findViewById(R.id.setting);
        final TextView tv5=(TextView)findViewById(R.id.laff);
        tv5.setTextColor(getResources().getColor(R.color.selected_item_color));

        lt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Profile", "profile");

                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_selected));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));

                tv.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i=new Intent(getBaseContext(),Profile.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();


            }
        });
        LinearLayout lt1=(LinearLayout)findViewById(R.id.make_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.guillo), "You are already here !",
                        Snackbar.LENGTH_LONG);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.gu));
                snackbar.show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp_selected));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));

                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));
            }
        });
        LinearLayout lt4=(LinearLayout)findViewById(R.id.feed_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);


        lt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Snackbar.make(findViewById(R.id.guillo), "My Complaints !", Snackbar.LENGTH_LONG)
                //          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed_selected));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));

                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i=new Intent(getBaseContext(),MyComplaints.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt2=(LinearLayout)findViewById(R.id.settings_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getBaseContext(),LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt3=(LinearLayout)findViewById(R.id.activity_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //    Snackbar.make(findViewById(R.id.guillo), "Home !", Snackbar.LENGTH_LONG)
                //            .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity_active));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp));

                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.selected_item_color));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.white));

                Intent i = new Intent(getBaseContext(), Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        });
        LinearLayout lt5=(LinearLayout)findViewById(R.id.laf_group);
        //final ImageView ii=(ImageView)findViewById(R.id.actt);
        lt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Snackbar.make(findViewById(R.id.guillo), "You are already here !", Snackbar.LENGTH_LONG)
                          .setAction("Action", null).show();
                Log.d("Profile", "profile");
                ii.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                ii1.setImageDrawable(getResources().getDrawable(R.drawable.ic_create_white_48dp));
                ii2.setImageDrawable(getResources().getDrawable(R.drawable.ic_feed));
                ii3.setImageDrawable(getResources().getDrawable(R.drawable.ic_activity));
                ii4.setImageDrawable(getResources().getDrawable(R.drawable.lo));
                ii5.setImageDrawable(getResources().getDrawable(R.drawable.ic_archive_white_48dp_selected));

                tv.setTextColor(getResources().getColor(R.color.white));
                tv4.setTextColor(getResources().getColor(R.color.white));
                tv2.setTextColor(getResources().getColor(R.color.white));
                tv1.setTextColor(getResources().getColor(R.color.white));
                tv3.setTextColor(getResources().getColor(R.color.white));
                tv5.setTextColor(getResources().getColor(R.color.selected_item_color));

            }
        });






    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed()
    {
        Intent i=new Intent(this,Home.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        finish();
    }
}
