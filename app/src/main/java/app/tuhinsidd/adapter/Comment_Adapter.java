package app.tuhinsidd.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import app.tuhinsidd.R;

public class Comment_Adapter extends ArrayAdapter<Comment_Item> {
    private static final String LOG_TAG = Comment_Adapter.class.getSimpleName();

    /**
     * This is our own custom constructor (it doesn't mirror a superclass constructor).
     * The context is used to inflate the layout file, and the List is the data we want
     * to populate into the lists
     *
     * @param context        The current context. Used to inflate the layout file.
     * @param commentItems A List of Comment_Item objects to display in a list
     */
    public Comment_Adapter(Activity context, List<Comment_Item> commentItems) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, commentItems);
    }

    /**
     * Provides a view for an AdapterView (ListView, GridView, etc.)
     *
     * @param position    The AdapterView position that is requesting a view
     * @param convertView The recycled view to populate.
     *                    (search online for "android view recycling" to learn more)
     * @param parent The parent ViewGroup that is used for inflation.
     * @return The View for the position in the AdapterView.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Gets the Comment_Item object from the ArrayAdapter at the appropriate position
        Comment_Item commentItem = getItem(position);

        // Adapters recycle views to AdapterViews.
        // If this is a new View object we're getting, then inflate the layout.
        // If not, this view already has the layout inflated from a previous call to getView,
        // and we modify the View widgets as usual.
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.comment_item, parent, false);
        }

        TextView NameView = (TextView) convertView.findViewById(R.id._name);
        NameView.setText(commentItem.name);


        TextView CommentView = (TextView) convertView.findViewById(R.id._comment);
        CommentView.setText(commentItem.comment);


        TextView TimeView = (TextView) convertView.findViewById(R.id._time);
        TimeView.setText(commentItem.time);


        return convertView;
    }
}
