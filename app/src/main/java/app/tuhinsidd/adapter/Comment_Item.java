package app.tuhinsidd.adapter;

/**
 * Created by poornima-udacity on 6/26/15.
 */
public class Comment_Item {
    String name;
    String comment;
    String time; // drawable reference id

    public Comment_Item(String Name, String comment, String time)
    {
        this.name = Name;
        this.comment = comment;
        this.time = time;
    }

}